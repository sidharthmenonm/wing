
$('.speaker').on('click', function(){
  var speaker = $(this);
  if(speaker.children('.bio').length>0){
    var image = speaker.find('img').attr('src') || '';
    var name = speaker.find('h4').text();
    var desi = speaker.find('h5.desi').text() + ', ' + speaker.find('h5.com').text();
    var bio = speaker.find('.bio').html();
    console.log(bio)
    var modal = $('#bio-modal');
    
    modal.find('img').attr('src', image);
    modal.find('h3').text(name);
    modal.find('h4').text(desi);
    modal.find('.content').html(bio);
    
    modal.modal('show');
  }
});

